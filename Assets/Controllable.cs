﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controllable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    Dictionary<KeyCode, Vector2> directions = new Dictionary<KeyCode, Vector2>() { 
        {KeyCode.LeftArrow, Vector2.left },
        {KeyCode.RightArrow, Vector2.right },
        {KeyCode.UpArrow, Vector2.up },
        {KeyCode.DownArrow, Vector2.down }
    };

    void ControlsKeyDown()
    {
        Animator anim = GetComponent<Animator>();
        bool walking_bool = false;

        foreach (KeyValuePair<KeyCode, Vector2> kv in directions)
        {
            if (Input.GetKeyDown(kv.Key))
            {
                transform.position += (Vector3)kv.Value;
                walking_bool = true;
            }
        }

        anim.SetBool("walking", walking_bool);
    }
    public float speed = 2f;
    void ControlsKey()
    {
        Animator anim = GetComponent<Animator>();
        bool walking_bool = false;

        foreach (KeyValuePair<KeyCode, Vector2> kv in directions)
        {
            if (Input.GetKey(kv.Key))
            {
                transform.position += (Vector3)kv.Value * Time.deltaTime * speed;
                walking_bool = true;
            }
        }

        anim.SetBool("walking", walking_bool);
    }

    // Update is called once per frame
    void Update()
    {
        ControlsKey();
        //ControlsKeyDown();

        return;

        #region Old more verbose version
        /*Vector2 left = new Vector2(-1f, 0f);
        left = Vector2.left;

        Animator anim = GetComponent<Animator>();
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Vector2 pos = transform.position;
            pos.x = pos.x - 1;
            transform.position = pos;
            anim.SetBool("walking", true);
        }
        else
        {
            anim.SetBool("walking", false);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Vector2 pos = transform.position;
            pos.x = pos.x + 1;
            transform.position = pos;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Vector2 pos = transform.position;
            pos.y = pos.y + 1;
            transform.position = pos;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Vector2 pos = transform.position;
            pos.y = pos.y - 1;
            transform.position = pos;
        }*/
        #endregion
    }
}
